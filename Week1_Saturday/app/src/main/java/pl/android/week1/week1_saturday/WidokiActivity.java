package pl.android.week1.week1_saturday;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class WidokiActivity extends ActionBarActivity {

    private static final String TAG = WidokiActivity.class.getName();

    private EditText mEditText;
//    private TextView mTextView;
    private Button mButton;
    private LinearLayout mLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) { //jaki widok dla aktywności
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            // Pierwsze otwarcie
        } else {
            // obrót ekranu, zmiana języka w systemie, restart systemu
        }

        // ustawienie widoku jako treść z pliku activity_widoki.xml
        setContentView(R.layout.activity_widoki);

        // findViewById musi być koniecznie po setContentView
        mEditText = findView(R.id.pole_tekstowe);   // wykonanie metody rzutującej
//        mTextView = (TextView) findViewById(R.id.label);
        mButton = (Button) findViewById(R.id.przycisk);
        mLinear = findView(R.id.teksty_kontener);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                klikPrzycisk(v);
            }
        });

        // wygenerowanie widoku z kodu
        //widokZKodu();
    }

    public void klikPrzycisk(View view) {
        String text = mEditText.getText().toString();
        LinearLayout.LayoutParams mKontrolkaParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        String imieHint = getResources().getString(R.string.tooShortToastText);

        // logowania w trybie debug
        Log.d(TAG, text != null ? "Tekst ma długość " + text.length() : "Null !");

        if (text == null || text.length() < 3) {
            // Wyświetl ze za krótki tekst
            Toast.makeText(this, text.isEmpty() ? "Nic nie wpisałeś" : imieHint, Toast.LENGTH_SHORT).show();
            return;
        }
        mEditText.setText(""); // czyscimy pole edycyjne
//        mTextView.setText(text);
        TextView mNowePole = new TextView(this);
        mNowePole.setLayoutParams(mKontrolkaParams);
        mNowePole.setText(text);

        mLinear.addView(mNowePole);
    }

    // generowanie widoku z kodu
    public void widokZKodu() {
        mLinear = new LinearLayout(this);
        mLinear.setGravity(Gravity.CENTER_HORIZONTAL);
        mLinear.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        mLinear.setLayoutParams(mParams);

        // wspólne atrybuty szerokości i wysokości dla pola tekstowego i edytowalnego
        LinearLayout.LayoutParams mKontrolkaParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mEditText = new EditText(this);
        mEditText.setLayoutParams(mKontrolkaParams);
        mEditText.setSingleLine(true);
        mEditText.setHint(R.string.imie_hint);
//        mEditText.setHint("Wpisz tutaj swoje imię");
        // setId() nie jest potrzebne w przypadku tworzenia widoku z kodu

//        mTextView = new TextView(this);
//        mTextView.setLayoutParams(mKontrolkaParams);

        LinearLayout.LayoutParams mGuzikParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mButton = new Button(this);
        mButton.setLayoutParams(mGuzikParams);
        mButton.setText("Gotowe");
        // preferowany sposób ustawiania metody pod przyciskiem
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                klikPrzycisk(v);
            }
        });

        // dodanie kontrolek do kontenera linearLayout
        mLinear.addView(mEditText);
//        mLinear.addView(mTextView);
        mLinear.addView(mButton);

        // wstawienie do kontenera obecnego w pliku XML
//        ((RelativeLayout) findViewById(R.id.kontener)).addView(mLinear);
        // ustawienie całej treści na wygenerowany widok
        setContentView(mLinear); // dla tego przypadku nie potrzebny plik .xml
    }

    // Metoda wykonująca rzutowanie
    public <T extends View> T findView(int identyfikator) {
        return (T) findViewById(identyfikator);
    }

//    metody do obsługi menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_widoki, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.usun_wszystko) {
            // dynamiczne usuwanie komponentów z widoku
            wyczyscWszystkie();
            return true;
        }
        if (id == R.id.usun_ostatni) {
            // dynamiczne usuwanie komponentów z widoku
            wyczyscOstatni();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void wyczyscWszystkie() {
        List<View> mKolekcjaDoUsuniecia = new ArrayList<View>();
        for ( int i = 0; i < mLinear.getChildCount(); i++ ) {
            View mChild = mLinear.getChildAt(i);
            mKolekcjaDoUsuniecia.add(mChild);
        }
        for (View mChild : mKolekcjaDoUsuniecia) {
            if (mChild.getClass().equals(TextView.class)) {
                if (mChild instanceof TextView && !(mChild instanceof EditText) && !(mChild instanceof Button)) {
                    mLinear.removeView(mChild);
                }
            }
        }
    }

    protected void wyczyscOstatni() {
        View mLastElement = mLinear.getChildAt(mLinear.getChildCount() - 1);
        if (mLastElement.getClass().equals(TextView.class)) {
            if (mLastElement instanceof TextView && !(mLastElement instanceof EditText) && !(mLastElement instanceof Button)) {
                mLinear.removeView(mLastElement);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
