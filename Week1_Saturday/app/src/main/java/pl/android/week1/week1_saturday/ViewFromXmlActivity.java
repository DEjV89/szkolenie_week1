package pl.android.week1.week1_saturday;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import pl.android.week1.week1_saturday.R;


public class ViewFromXmlActivity extends ActionBarActivity {

    private static final String TAG = ViewFromXmlActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widoki);
        Log.d(TAG, "--Metoda onCreate()--");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "--Metoda onStart()--");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "--Metoda onResume()--");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "--Metoda onPause()--");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "--Metoda onStop()--");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "--Metoda onDestroy()--");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "--Metoda onRestart()--");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_widoki, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
